install:
	pip3 install --upgrade -e .

uninstall:
	pip3 uninstall laptop-detect --yes

clean:
	rm -rfv build/
	rm -rfv dist/
	rm -rfv *.egg-info/

build_dist: clean
	python3 setup.py sdist bdist_wheel
	twine check dist/*

deploy: build_dist
	twine upload dist/*

deploy_testing: build_dist
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*
