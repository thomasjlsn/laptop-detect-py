# laptop-detect (python port)

This package provides a function that is a port of the popular
[laptop-detect](https://gitlab.com/debiants/laptop-detect) shell script.

## Usage

Basic usage:
```
from laptop_detect import laptop_detect

on_laptop = laptop_detect()

if on_laptop:
    print("We're on a laptop")
```

Check the reason why the device is believed to be a laptop:
```
from laptop_detect import laptop_detect, reason

on_laptop = laptop_detect()

if on_laptop:
    print(reason[on_laptop])
```
